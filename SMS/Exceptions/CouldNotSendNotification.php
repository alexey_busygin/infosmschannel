<?php

namespace App\Channels\SMS\Exceptions;

use Exception;
use GuzzleHttp\Exception\ClientException;

/**
 * Class CouldNotSendNotification.
 */
class CouldNotSendNotification extends Exception
{
    /**
     * Thrown when there's a bad request and an error is responded.
     *
     * @param ClientException $exception
     *
     * @return static
     */
    public static function serviceRespondedWithAnError(ClientException $exception): self
    {
        if (! $exception->hasResponse()) {
            return new static('Service responded with an error but no response body found');
        }

        $statusCode = $exception->getResponse()->getStatusCode();

        $result = json_decode($exception->getResponse()->getBody(), false);
        $description = $result->description ?? 'no description given';

        return new static("Service responded with an error `{$statusCode} - {$description}`");
    }

    /**
     * Thrown when there's no login or password provided.
     *
     * @param string $message
     *
     * @return static
     */
    public static function serviceCredentialsNotProvided($message): self
    {
        return new static($message);
    }

    /**
     * Thrown when there's no params to set.
     *
     * @param string $message
     *
     * @return static
     */
    public static function serviceParamsEmpty($message): self
    {
        return new static($message);
    }

    /**
     * Thrown when we're unable to communicate with Service.
     *
     * @param $message
     *
     * @return static
     */
    public static function couldNotCommunicateWithService($message): self
    {
        return new static("The communication with service failed. `{$message}`");
    }


    /**
     * Thrown when there is no phone number provided.
     *
     * @return static
     */
    public static function phoneNumberNotProvided(): self
    {
        return new static('User phone number was not provided.');
    }
}
