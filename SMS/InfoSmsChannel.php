<?php


namespace App\Channels\SMS;

use Exception;
use Illuminate\Notifications\Notification;
use App\Channels\SMS\Exceptions\CouldNotSendNotification;
use Illuminate\Support\Facades\Log;

class InfoSmsChannel
{

    /**
     * @var InfoSms $service
     */
    protected $service;

    /**
     * Channel constructor.
     *
     * @param InfoSms $service
     */
    public function __construct(InfoSms $service)
    {
        $this->service = $service;
    }

    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toInfoSms($notifiable);

        $to = $notifiable->routeNotificationFor('InfoSms');

        if(!$to) {
            throw CouldNotSendNotification::phoneNumberNotProvided();
        }

        $from = config('services.infosms.from');

        if (is_string($message)) {
            $message = urlencode((new InfoSmsMessage)->content($message));
        }

        $params = [
            'phones' => $to,
            'message' => $message,
            'sender' => $from,
            //'getcost' => 1, /*For tests*/
        ];

        try {
            $this->service->sendMessage($params);
        } catch (CouldNotSendNotification $exception) {
            Log::channel('sms')->info('Невозможно отправить сообщение'.$exception->getMessage());
        } catch (Exception $exception) {
            Log::channel('sms')->info('Ошибка при отправке смс'.$exception->getMessage());
        }

    }
}
