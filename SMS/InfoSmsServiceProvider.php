<?php


namespace App\Channels\SMS;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\ServiceProvider;

class InfoSmsServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     */
    public function boot(): void
    {
        $this->app->when(InfoSmsChannel::class)
                  ->needs(InfoSms::class)
                  ->give(static function () {
                      return new InfoSms(
                          config('services.infosms.login'),
                          config('services.infosms.password'),
                          new HttpClient()
                      );
                  });
    }

}
