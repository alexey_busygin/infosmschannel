<?php


namespace App\Channels\SMS;


class InfoSmsMessage
{
    public $message;

    public function content($message)
    {
        $this->message = $message;

        return $this->message;
    }

}
