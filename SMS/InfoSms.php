<?php


namespace App\Channels\SMS;

use Exception;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use App\Channels\SMS\Exceptions\CouldNotSendNotification;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

class InfoSms
{
    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var HttpClient
     */
    private $http;

    public function __construct($login = null, $password = null, HttpClient $httpClient = null)
    {

        $this->login = $login;
        $this->password = $password;
        $this->http = $httpClient;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * Get HttpClient.
     *
     * @return HttpClient
     */
    protected function httpClient(): HttpClient
    {
        return $this->http ?? new HttpClient();
    }


    public function sendMessage(array $params): ?ResponseInterface
    {
        return $this->sendRequest('sendMessage', $params);
    }


    protected function sendRequest(string $endpoint, array $params): ?ResponseInterface
    {
        if (blank($this->login) || blank($this->password)) {
            throw CouldNotSendNotification::serviceCredentialsNotProvided('You must provide your credentials to make any API requests.');
        }

        $endPointUrl = 'http://api.infosmska.ru/interfaces/'.$endpoint.'.ashx?login='.$this->login.'&pwd='.$this->password;

        if( !count($params) ) {
            throw CouldNotSendNotification::serviceParamsEmpty('You must pass params to make any API requests.');
        }

        foreach ($params as $key => $param) {
            $endPointUrl .= '&'.$key.'='.$param;
        }
        try {
            $res = $this->httpClient()->get($endPointUrl);
            try {
                Log::info($res->getBody());
            } catch (Exception $e){
                Log::error($e->getMessage());
            }
            return $res;
        } catch (ClientException $exception) {
            throw CouldNotSendNotification::serviceRespondedWithAnError($exception);
        } catch (Exception $exception) {
            throw CouldNotSendNotification::couldNotCommunicateWithService($exception);
        }
    }

}
